get '/users' do
  u = User.first(10)

  u.to_json
end

get '/users/first' do
  u = User.first

  u.to_json
end

get '/users/:id' do
  u = User.all(conditions: ['id = ?', params[:id]])

  u.to_json
end
